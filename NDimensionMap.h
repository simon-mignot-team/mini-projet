//
// Created by Simon Mignot on 26/12/17.
//

#ifndef MINI_PROJET_NDIMENSIONARRAY_H
#define MINI_PROJET_NDIMENSIONARRAY_H

#include <vector>
#include <iostream>
#include <map>

/*
 * Helper class
 * Store doubles in a n-dimension array, and provide min, max, average function on it.
 */

class NDimensionMap : std::map<std::vector<int>, double>
{
    public:
        NDimensionMap(int dimension, int dimensionsSize);

        class CombinationCounter;

        void display(std::string title = "Array");

        NDimensionMap::CombinationCounter createCombinationCounter(std::vector<std::vector<int>> filter = std::vector<std::vector<int>>());

        std::vector<int> getDimensions() const;
        double& operator[](std::vector<int> indexes);

        std::vector<std::vector<int>> getMaximumsPositions();
        double getMaximumValue(NDimensionMap::CombinationCounter counter);
        double getMinimumValue(NDimensionMap::CombinationCounter counter);
        double getAverageValue(NDimensionMap::CombinationCounter counter);

    private:
        std::vector<int> m_dimensions;
};



class NDimensionMap::CombinationCounter : public std::vector<int>
{
    public:
        CombinationCounter(std::vector<int> dimensions, std::vector<std::vector<int>> filter = std::vector<std::vector<int>>());

        int next();
        NDimensionMap::CombinationCounter& operator++();

        void display() { for(auto&x:m_counter) std::cout << x << ' '; std::cout << '\t'; for(auto&x:(*this)) std::cout << x << ' '; }

    private:
        bool isFiltered();

        std::vector<std::vector<int>> m_filter;
        std::vector<int> m_counter;
        int m_lastModified;
};

#endif //MINI_PROJET_NDIMENSIONARRAY_H
