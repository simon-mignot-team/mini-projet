//
// Created by Simon Mignot on 26/12/17.
//

#ifndef MINI_PROJET_CLUSTERING_H
#define MINI_PROJET_CLUSTERING_H


#include <vector>
#include "NDimensionMap.h"

typedef std::vector<int> Cluster;

class Clustering
{
    public:
        Clustering(std::vector<std::vector<int>> data);

        /*
         * Main function, return the m_clusters attribute once it has been processed.
         */
        std::vector<Cluster> calculateCluster(int clusterCount);

        /*
         * Display min, max and average for each cluster and between all clusters
         */
        void displayClustersInfos();

    private:
        /*
         * (Re)Initialize the clustering.
         */
        void initClustering(int clusterCount);

        /*
         * Generates a 2D array with the distance for each example
         *   against each other and store it as attribute (m_hamming2d).
         */
        void calculate2DHamming();

        /*
         * Determine the starting clusters, as the N examples have to be as far apart as possible.
         *
         * Chooses the N furthest examples to start clusters and places them in the m_clusters attribute.
         */
        void calculateStartClusters();

        /*
         * Determine which examples could be the next to be placed, per cluster.
         *
         * For each remaining examples :
         *     - Calculate the average distance with every cluster.
         *
         * The return value is an array representing the clusters, with N elements each, or an empty
         *   array when the clustering is finished.
         * Each "cluster-array" is filled with the upper bound of a double, and the needed data calculated
         *   and placed at the right indice.
         */
        std::vector<std::vector<double>> getDistancesFromClusters();

        /*
         * With the output of getDistancesFromClusters :
         *     - Choose one example per cluster (as they have the same value per cluster)
         *     - Calculate the minimums of all clusters
         *     - Add the one who best fit to its cluster
         *     - Remove the example from the m_data.
         */
        void addMinimumToCluster(std::vector<std::vector<double>> distances);

        /*
         * Calculate the pertinence for each possible set of examples.
         */
        NDimensionMap calculatePertinence();

        /*
         * Calculate the average distance for each possible set of examples.
         * Utilized for displaying clusters infos.
         */
        NDimensionMap calculateAverageDistance(int dimension, std::vector<std::vector<int>> indexes);

        /*
         * Add each combination of 2 from counter from m_hamming2d.
         */
        std::vector<double> getHammingCombinationValue(NDimensionMap::CombinationCounter counter);

        NDimensionMap m_hamming2d;
        std::vector<std::vector<int>> m_rawData;
        std::vector<std::vector<int>> m_data;
        std::vector<std::vector<int>> m_clusters;
};


#endif //MINI_PROJET_CLUSTERING_H
