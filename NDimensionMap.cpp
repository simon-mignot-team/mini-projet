//
// Created by Simon Mignot on 26/12/17.
//

#include <cmath>
#include <algorithm>
#include "NDimensionMap.h"


NDimensionMap::NDimensionMap(int dimension, int dimensionsSize)
{
    for(int i = 0; i < dimension; ++i)
        m_dimensions.push_back(dimensionsSize);
}

void NDimensionMap::display(std::string title)
{
    std::cout << "Dimensions: ";
    for(int i = 0; i < m_dimensions.size(); ++i)
        std::cout << m_dimensions[i] << ' ';
    std::cout << '\n';

    std::cout << title << ":\n";
    /*for(auto it = this->begin(); it != this->end(); ++it)
    {
        for(auto& x : it->first)
            std::cout << x << " ";
        std::cout << ": " << it->second << '\n';
    }*/

    for(int y = 0; y < m_dimensions[0]; ++y)
    {
        for(int x = 0; x < m_dimensions[0]; ++x)
            std::cout << (*this)[{x, y}] << '\t';
        std::cout << '\n';
    }
}

double& NDimensionMap::operator[](std::vector<int> indexes)
{
    if(indexes.size() > m_dimensions.size())
        throw std::runtime_error("Too much dimensions (max " + std::to_string(m_dimensions.size()) + ").");
    return std::map<std::vector<int>, double>::operator[](indexes);
}

NDimensionMap::CombinationCounter NDimensionMap::createCombinationCounter(std::vector<std::vector<int>> filter)
{
    return NDimensionMap::CombinationCounter(m_dimensions, filter);
}

std::vector<int> NDimensionMap::getDimensions() const
{
    return m_dimensions;
}

std::vector<std::vector<int>> NDimensionMap::getMaximumsPositions()
{
    std::vector<std::vector<int>> result;
    std::map<std::vector<int>, double>::iterator current = this->begin();
    double max = 0;
    int i = 0;
    do
    {
        if(current != this->begin())
            ++current;
        // Use lambda expression, as map::operator<() compare keys, not values
        current = std::max_element(current, this->end(),
                                   [](const std::pair<std::vector<int>, double>& r, const std::pair<std::vector<int>, double>& l) { return r.second < l.second; });

        if(current == this->end() || current->second < max)
            break;
        else
            max = current->second;

        result.push_back(current->first);
        if(current == this->begin())
            ++current;
    } while(current->second >= max || current != this->end());

    return result;
}

double NDimensionMap::getMaximumValue(NDimensionMap::CombinationCounter counter)
{
    double max = (*this)[counter];
    do
    {
        if(max < (*this)[counter])
            max = (*this)[counter];
    } while((counter.next()));
    return max;
}

double NDimensionMap::getMinimumValue(NDimensionMap::CombinationCounter counter)
{
    double min = (*this)[counter];
    do
    {
        if((*this)[counter] < min)
            min = (*this)[counter];
    } while((counter.next()));
    return min;
}

double NDimensionMap::getAverageValue(NDimensionMap::CombinationCounter counter)
{
    double avg = 0;
    int div = 0;
    do
    {
        ++div, avg += (*this)[counter];
    } while(counter.next());
    return avg / div;
}





NDimensionMap::CombinationCounter::CombinationCounter(std::vector<int> dimensions, std::vector<std::vector<int>> filter)
        : m_counter(dimensions[0]), std::vector<int>(dimensions.size()), m_filter(filter)
{
    for(int i = 0; i < dimensions.size(); ++i)
        m_counter[i] = 1;
    std::prev_permutation(m_counter.rbegin(), m_counter.rend());
    next();
}

int NDimensionMap::CombinationCounter::next()
{
    m_lastModified = 0;
    int result = 0;
    do
    {
        ++(*this);
        if(m_lastModified > result)
            result = m_lastModified;
    } while(!isFiltered() && m_lastModified);
    return m_lastModified ? result : 0;
}

bool NDimensionMap::CombinationCounter::isFiltered()
{
    if(m_filter.empty())
        return true;

    if(m_filter.size() != (*this).size())
        throw std::runtime_error("Filter size different from counter size.");
    for(int i = 0; i < m_filter.size(); ++i)
        if(std::find(m_filter[i].begin(), m_filter[i].end(), (*this)[i]) == m_filter[i].end())
            return false;

    return true;
}

NDimensionMap::CombinationCounter& NDimensionMap::CombinationCounter::operator++()
{
    bool finalPerm = !std::next_permutation(m_counter.rbegin(), m_counter.rend());
    int result = 0;
    for(int iPerm = 0, iCounter = 0; iPerm < m_counter.size(); ++iPerm)
    {
        if(m_counter[iPerm] == 1)
        {
            if((*this)[iCounter] != iPerm && result == 0)
                result = iCounter + 1;
            (*this)[iCounter++] = iPerm;
        }
    }
    m_lastModified = finalPerm ? 0 : result;
    return *this;
}