//
// Created by Simon Mignot on 26/12/17.
//

#include <algorithm>
#include <numeric>
#include "Clustering.h"
#include "Calculation.h"

Clustering::Clustering(std::vector<std::vector<int>> data) : m_rawData(data), m_hamming2d(0, 0)
{
    
}


void Clustering::initClustering(int clusterCount)
{
    m_clusters = std::vector<std::vector<int>>(clusterCount);
    m_data = m_rawData;
}

std::vector<Cluster> Clustering::calculateCluster(int clusterCount)
{
    initClustering(clusterCount);

    // Calculate hamming distance between each examples
    calculate2DHamming();

    // Initialize N clusters with the N most pertinent examples
    calculateStartClusters();
    for(int i = 0; i < m_clusters.size(); ++i) // Remove the first examples from data
        m_data[m_clusters[i][0]].clear();
    
    bool stop = false;
    while(!stop)
    {
        std::vector<std::vector<double>> distances = getDistancesFromClusters();

        stop = distances.empty();
        if(!stop)
            addMinimumToCluster(distances);
    }
    return m_clusters;
}

void Clustering::calculate2DHamming()
{
    m_hamming2d = NDimensionMap(2, m_data.size());                // Initialize the 2D array

    for(int y = 0; y < static_cast<int>(m_data.size()); ++y)    // For each example
        for(int x = 0; x < m_data.size(); ++x)                  // with each other
            for(int i = 0; i < m_data[x].size(); ++i)           // count the differences
                if(m_data[x][i] != m_data[y][i])
                    ++m_hamming2d[{x, y}];
}

void Clustering::calculateStartClusters()
{
    NDimensionMap pertinences = calculatePertinence();
    std::vector<std::vector<int>> maxes = pertinences.getMaximumsPositions(); // Get all set of examples with the max pertinence

    // TODO: choose randomly

    for(int i = 0; i < maxes[0].size(); ++i)    // Place the set into the N clusters
        m_clusters[i].push_back(maxes[0][i]);
}

std::vector<std::vector<double>> Clustering::getDistancesFromClusters()
{
    std::vector<std::vector<double>> result(m_clusters.size(), std::vector<double>(m_data.size(), std::numeric_limits<double>::max()));

    bool noData = true;
    for(int iData = 0; iData < m_data.size(); ++iData)          // For each example
    {
        if(m_data[iData].empty())
            continue;
        noData = false;
        for(int iClusters = 0; iClusters < m_clusters.size(); ++iClusters)  // For each cluster
        {
            double avg = 0;
            for(int i = 0; i < m_clusters[iClusters].size(); ++i)
                avg += m_hamming2d[{iData, m_clusters[iClusters][i]}];      // Calculate the average with cluster's element
            result[iClusters][iData] = avg / m_clusters[iClusters].size();  // Add it to the result array
        }
    }
    if(noData)
        return std::vector<std::vector<double>>();
    else
        return result;
}

void Clustering::addMinimumToCluster(std::vector<std::vector<double>> distances)
{
    std::vector<std::pair<double, int>> minimums(m_clusters.size()); // invert double and int to allow searching min_element on the double part
    for(int i = 0; i < m_clusters.size(); ++i)
    {
        // Search the minimum for this cluster
        std::vector<double>::iterator it = std::min_element(distances[i].begin(), distances[i].end());
        minimums[i].first = *it;
        minimums[i].second = std::distance(distances[i].begin(), it);
    }

    // TODO: choose randomly
    std::vector<std::pair<double, int>>::iterator minElement = std::min_element(minimums.begin(), minimums.end()); // Search the minimum among all clusters
    m_clusters[std::distance(minimums.begin(), minElement)].push_back(minElement->second); // Add the chosen exemple to the corresponding cluster
    m_data[minElement->second].clear(); // Remove the exemple from data
}

NDimensionMap Clustering::calculatePertinence()
{
    NDimensionMap result(m_clusters.size(), m_hamming2d.getDimensions()[0]);
    NDimensionMap::CombinationCounter counter = result.createCombinationCounter();
    do
    {
        std::vector<double> values = getHammingCombinationValue(counter);

        double avg = 0;
        if(!values.empty())
        {
            avg = std::accumulate(values.begin(), values.end(), 0.0) / values.size();
            result[counter] = avg * *std::min_element(values.begin(), values.end()); // avg * per; // Pertinence
        }
        else
            result[counter] = 1;
    } while(counter.next());
    return result;
}

NDimensionMap Clustering::calculateAverageDistance(int dimension, std::vector<std::vector<int>> indexes)
{
    NDimensionMap result(dimension, m_hamming2d.getDimensions()[0]);
    NDimensionMap::CombinationCounter counter = result.createCombinationCounter(indexes);
    do
    {
        std::vector<double> values = getHammingCombinationValue(counter);

        result[counter] = std::accumulate(values.begin(), values.end(), 0.0) / values.size();   // Average
    } while(counter.next());
    return result;
}
void Clustering::displayClustersInfos()
{
    {
        NDimensionMap interclusters = calculateAverageDistance(m_clusters.size(), m_clusters);
        NDimensionMap::CombinationCounter counter = interclusters.createCombinationCounter(m_clusters);

        std::cout << "Inter clusters distances :\n";
        std::cout << "min = " << interclusters.getMinimumValue(counter) << '\n';
        std::cout << "max = " << interclusters.getMaximumValue(counter) << '\n';
        std::cout << "avg = " << interclusters.getAverageValue(counter) << "\n\n";
    }

    for(int i = 0; i < m_clusters.size(); ++i)
    {
        NDimensionMap cluster = calculateAverageDistance(2, {m_clusters[i], m_clusters[i]});
        NDimensionMap::CombinationCounter counter = cluster.createCombinationCounter({m_clusters[i], m_clusters[i]});

        std::cout << "Cluster " << i << " :\n[ ";
        for(auto& x : m_clusters[i])
            std::cout << x << ' ';
        std::cout << "]\n";

        std::cout << "min = " << cluster.getMinimumValue(counter) << '\n';
        std::cout << "max = " << cluster.getMaximumValue(counter) << '\n';
        std::cout << "avg = " << cluster.getAverageValue(counter) << "\n\n";
    }
}

std::vector<double> Clustering::getHammingCombinationValue(NDimensionMap::CombinationCounter counter)
{
    std::vector<double> result;

    for(int i = 0; i < static_cast<int>(counter.size()) - 1; ++i) // counter.size() return a unsigned long and cause infinite loop if size is 0 (0 - 1 = 2^8 here)
        for(int j = i + 1; j < counter.size(); ++j)
            result.push_back(m_hamming2d[{counter[i], counter[j]}]);
    return result;
}