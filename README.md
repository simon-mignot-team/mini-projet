# Projet IA

Dépot original : https://bitbucket.org/simon-mignot-team/mini-projet

## Compilation

Trois exemples d'execution du code sont disponibles dans le fichier main.cpp : 
 - Benchmark (1000 executions en 2 clusters) (sans affichage)
 - Tous les clusters possibles (en 1-10 clusters) (avec affichage du resultat)
 - En 2 clusters (avec affichage du resultat)

### Linux

```
apt-get update
apt-get install build-essential cmake

unzip mini-projet.zip
cd mini-projet

mkdir build && cd build
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ..
make

./mini_projet
```

