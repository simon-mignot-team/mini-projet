#include <iostream>
#include <algorithm>
#include "NDimensionMap.h"
#include "Clustering.h"

int main()
{
    std::vector<std::vector<int>> data = { {0, 2, 2, 0},
                                           {0, 1, 2, 0},
                                           {1, 2, 2, 0},
                                           {0, 2, 1, 0},
                                           {0, 2, 2, 1},
                                           {0, 1, 1, 1},
                                           {1, 2, 2, 1},
                                           {1, 1, 1, 0},
                                           {1, 1, 1, 1},
                                           {1, 2, 1, 1},
                                           /*{1, 2, 2, 0},
                                           {0, 1, 2, 1},
                                           {1, 2, 2, 1},
                                           {0, 2, 1, 1},
                                           {0, 1, 1, 1},
                                           {0, 2, 1, 1},
                                           {1, 2, 1, 1},
                                           {1, 1, 1, 0},
                                           {1, 1, 1, 1},
                                           {1, 2, 2, 1}*/ };

    Clustering clustering(data);

    /// Benchmark
    /*
    for(int i = 0; i < 1000; ++i)
        clustering.calculateCluster(2); // 3
    //*/

    /// 1-10 clusters
    //*
    for(int i = 1; i < 11; ++i)
    {
        std::cout << "#### " << i << " clusters\n";
        clustering.calculateCluster(i);
        clustering.displayClustersInfos();
        std::cout << "\n\n";
    }
    //*/

    /// 2 clusters
    /*
    std::cout << "#### " << 2 << " clusters\n";
    clustering.calculateCluster(2);
    clustering.displayClustersInfos();
    //*/


    return 0;
}
