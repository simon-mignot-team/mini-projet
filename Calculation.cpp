//
// Created by Simon Mignot on 30/12/17.
//

#include <cmath>
#include <numeric>
#include "Calculation.h"

double Calculation::entropy(double r, double l)
{
    double total = r + l;
    double result = 0;
    r /= total;
    l /= total;
    if(r > 0)
        result -= r * std::log2(r);
    if(l > 0)
        result -= l * std::log2(l);
    return result;
}

double Calculation::pertinence(std::vector<double> values)
{
    double result = 0;
    int div = 0;
    double total = std::accumulate(values.begin(), values.end(), 0);
    for(int i = 0; i < values.size(); ++i)
        for(int j = i + 1; j < values.size(); ++j, ++div)
            result += entropy(values[i], values[j]);
    return result / div;
}
