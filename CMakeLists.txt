cmake_minimum_required(VERSION 3.5)
project(mini_projet)

set(CMAKE_CXX_STANDARD 11)

add_executable(mini_projet main.cpp NDimensionMap.cpp NDimensionMap.h Clustering.cpp Clustering.h Calculation.cpp Calculation.h)