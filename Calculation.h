//
// Created by Simon Mignot on 30/12/17.
//

#ifndef MINI_PROJET_CALCULATION_H
#define MINI_PROJET_CALCULATION_H


#include <vector>

class Calculation
{
    public:
        static double entropy(double r, double l);
        static double pertinence(std::vector<double> values);
};


#endif //MINI_PROJET_CALCULATION_H
